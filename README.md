# Robot Frameework

By following these fairly easy steps you can get the Robot Framework with Selenium2Library and SSHLibrary installed on Ubuntu 14.04. Now you could go and fetch the installation packages and for example compile everything from the scratch. That’s all just fine with ,e. Anyhow, you could also use these instructions and slip all the hassle.

First of all, to install everything, you will need to have pip. Pip is a Python package installer and it helps you out a lot in case you need to install anything related to Python.

Install pip
sudo apt-get install python-pip
Install Robot Framework
sudo pip install robotframework
Install Selenium2Library
sudo pip install robotframework-selenium2library
Install SSHLibrary
sudo pip install robotframework-sshlibrary
Now that was all I needed for getting started. However modifying the Robot Framework Test Cases from a text editor is a tricky business. So it would be wise to either use LibreOffice Calc or some other software that can read Tab Separated -files. There’s also a bunch of plugins for vim, Emacs, Sublime and Eclipse (or LiClipse) to get the TSV -format highlighted in the editor. The other way to do it is to use Robot Framework IDE, called RIDE. RIDE is indeed a decent tool for handling the keywords and variables in a correct format. The installation procedure, however, was not at all that straightforward as you might think. While you can install the RIDE with pip, you still need to have wxPython in order to run it.

Install RIDE
sudo pip install robotframework-ride
Install wxWidgets (original instructions in wxWiki)
sudo apt-get install libwxgtk2.8-dev libwxgtk2.8-dbg
sudo apt-get install build-essential
sudo apt-get install python-wxtools python-wxgtk2.8-dbg
Now you can get the ride started on command line by easily writing ride.py on the terminal. Happy trails  Next article should be about installing the whole stuff to Windows. Which seems to be a whole lot harder than it should be.


***************************
ubuntu 16.04
***************************

sudo add-apt-repository ppa:nilarimogard/webupd8

****************************************
To set the driver path
****************************************
chmod +x geckodriver(driver)
cp geckodriver /usr/local/bin/

